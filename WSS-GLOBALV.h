//#define DEBUG_ESP_ON 1
#ifdef DEBUG_ESP_ON
#define SERIAL_DEBUG(...) Serial.printf( __VA_ARGS__ )
#else
#define SERIAL_DEBUG(...)
#endif

//#define DEBUG_BLE_ON 1
#ifdef DEBUG_BLE_ON
#define SERIAL_BLE_SEND(...) Serial.printf( __VA_ARGS__ )
#else
#define SERIAL_BLE_SEND(...)
#endif
// when using nodeMCU as main board
//#define DEBUG_NODE_MCU 1

#define USING_AXTLS
#include <ESP8266WiFi.h>
#include <WiFiClientSecureAxTLS.h>
using namespace axTLS;

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored  "-Wdeprecated-declarations"
  WiFiClientSecure sClient;
#pragma GCC diagnostic pop

#include <ArduinoJson.h>

/*
   reference link
   https://www.arduino.cc/reference/en/language/variables/utilities/progmem/
*/
const char* firmwareVersionCode = "20190731b";
static bool configRobotWireDebug = false;
static bool configRobotBleMode = false;

# define API_REQUEST_URL "app.kaisclan.ai"
# define API_REQUEST_PORT 443
/*
   classroom & robot id related
 * *************************************************************************************************
*/
#define CLASSROOM_CODE_SIZE 6
#define UDP_CLASSROOM_SIZE 5
uint8_t udpClassroomCodeSavedCount = 0;
String udpClassroomCodeBuffer[UDP_CLASSROOM_SIZE];
uint8_t udpClassroomCodeBufferIndex = 0;
IPAddress udpServerList[UDP_CLASSROOM_SIZE];
IPAddress assignedUdpIpAddress;

#define ROBOT_ID_MAX 16
static String assignedClassroomCode = "";
static int assignedRobotId = 0;

/*
   MINI board related
 * *************************************************************************************************
*/
struct cRGB  {
  volatile uint8_t g;
  volatile uint8_t r;
  volatile uint8_t b;
};
struct cRGB colorHeadlights[2];
uint8_t colorHeadlightsBrightness = 0;
uint16_t colorHeadlightsOnTime = 0;
uint16_t colorHeadlightsOffTime = 0;

// count,, for testing only
uint8_t msg_count = 0;
uint8_t msg_count_feedback = 0;


#define AD_VALUE_USB_CHARGING 128
#define AD_VALUE_BATTERY_OPEN 168
#define AD_VALUE_BATTERY_ZERO 93
uint8_t batteryVoltageValue = 0;
uint8_t usbVoltageValue = 0;

#define MOTOR_BUFFER_LEN 4
struct steppermotor_type
{
  uint8_t directionLeft[MOTOR_BUFFER_LEN];
  uint8_t speedLeft[MOTOR_BUFFER_LEN];
  //uint16_t stepLeft[MOTOR_BUFFER_LEN];
  uint8_t directionRight[MOTOR_BUFFER_LEN];
  uint8_t speedRight[MOTOR_BUFFER_LEN];
  uint16_t steps[MOTOR_BUFFER_LEN];
};
steppermotor_type stepperMotorBuffer;
String commandIdSavedNormal = "";
uint8_t commandIndexSavedNormal = 0;
String commandIdSavedInterrupt = "";
uint8_t commandIndexSavedInterrupt = 0;

/*
   flow control related
 * *************************************************************************************************
*/
volatile uint8_t externalInterruptFlag = 1;
//commandStatusAsync = 0, idle
//commandStatusAsync = 2, send [finished command] to UI, then set to 0
//commandStatusAsync = 3, rtttl start running
//commandStatusAsync = 4, stepper motor start running
//commandStatusAsync = 5, stepper motor running finished
//commandStatusAsync = 6, pulse sensor start sampling

uint8_t commandStatusAsync = 0;
long timeLastInterruptSent = 0;
long timeLastMotorFinished = 0;
uint16_t sensorPublishInterval = 5000;

uint8_t sensorRefreshFlag = 3;
uint16_t sensorRefreshInterval = 500;
uint16_t sensorI2CInterval = 500;
#define MOTOR_COMMAND_DELAY 500
#define IR_RECOVERY_DELAY 500
#define INTERRUPT_RECOVERY_DELAY 500
/*
   general functions
 * *************************************************************************************************
   32 bit code generation
*/
const char dict32B[32] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5'};
String uint16ArrayToString(volatile uint16_t a[], uint8_t arraySize) {
  char g[arraySize * 2 + 1];
  g[arraySize * 2] = '\0';
  for (uint8_t i = 0; i < arraySize; i++) {
    g[i * 2] = dict32B[a[i] / 32];
    g[i * 2 + 1] = dict32B[a[i] % 32];
  }
  return String(g);
}
/*
   general functions
 * *************************************************************************************************
   internet time
*/

/*
uint8_t timeDay = 0;
uint8_t timeMonth = 0;
uint8_t timeWeek = 0;
uint16_t timeYear = 0;
uint8_t timeHour = 0;
uint8_t timeMinute = 0;
uint8_t timeSecond = 0;
uint8_t timeZone = 0;
const String monthKeyWords[12] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
const String weekKeyWords[7] = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
*/
// from "Mon, 08 Apr 2019 07:01:39 GMT" to arrays
/*
void timeString2Array(String timeString) {
  int i;
  i = timeString.indexOf(" ");
  if (i > 0 && i < timeString.length() - 3) {
    String weekStr = timeString.substring(0, i);
    for (uint8_t j = 0; j < 7; j++) {
      if (weekStr.indexOf(weekKeyWords[j]) >= 0) {
        timeWeek = j;
        break;
      }
    }
    timeString = timeString.substring(i + 1);
    //SERIAL_DEBUG("[TIME] s 1 = %s\n", timeString.c_str());
  }
  i = timeString.indexOf(" ");
  if (i > 0 && i < timeString.length() - 3) {
    timeDay = timeString.substring(0, i).toInt();
    timeString = timeString.substring(i + 1);
    //SERIAL_DEBUG("[TIME] s 2 = %s, day = %d\n", timeString.c_str(), timeDay);
  }
  i = timeString.indexOf(" ");
  if (i > 0 && i < timeString.length() - 3) {
    String monthStr = timeString.substring(0, i);
    for (uint8_t j = 0; j < 12; j++) {
      if (monthStr.indexOf(monthKeyWords[j]) >= 0) {
        timeMonth = j;
        break;
      }
    }
    timeString = timeString.substring(i + 1);
    //SERIAL_DEBUG("[TIME] s 3 = %s, month = %d\n", timeString.c_str(), timeMonth);
  }
  i = timeString.indexOf(" ");
  if (i > 0 && i < timeString.length() - 3) {
    timeYear = timeString.substring(0, i).toInt();
    //SERIAL_DEBUG("[TIME] s t = %s, year = %d\n", yearStr.c_str(),String("2019").toInt());
    timeString = timeString.substring(i + 1);
    //SERIAL_DEBUG("[TIME] s 4 = %s, year = %d\n", timeString.c_str(), timeYear);
  }
  i = timeString.indexOf(" ");
  if (i > 0 && i < timeString.length() - 3) {
    String timeT = timeString.substring(0, i);
    int t;
    t = timeT.indexOf(":");
    timeHour = timeT.substring(0, t).toInt();
    timeT = timeT.substring(t + 1);
    t = timeT.indexOf(":");
    timeMinute = timeT.substring(0, t).toInt();
    timeT = timeT.substring(t + 1);
    timeSecond = timeT.toInt();
    timeString = timeString.substring(i + 1);
    //SERIAL_DEBUG("[TIME] s 5 = %s, hour = %d, minute = %d, second = %d\n", timeString.c_str(), timeHour, timeMinute, timeSecond);
  }
  SERIAL_DEBUG("[TIME] time is year = %d, month index = %d, day = %d, hour = %d, minute = %d, second = %d, week index = %d\n", timeYear, timeMonth, timeDay, timeHour, timeMinute, timeSecond, timeWeek);
}
*/

