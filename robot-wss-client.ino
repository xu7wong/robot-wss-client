/*
   created on 2019 Mar 28
*/
#include "WSS-GLOBALV.h" // global variables here

#include "WSS-EEPROM.CPP" // EEPROM config here

#include "WSS-GPIO.CPP" // gpio config

#include "WSS-INTERFACE.CPP" // interfaces here

#include "WSS-WIFI.CPP" // WiFi config
#ifdef DEBUG_BLE_ON
#include "WSS-BLE.CPP" // EEPROM config here
#endif
#include "WSS-UPDATE.CPP" // robot LAN webpage firmware update

#include "WSS-MQTT.CPP" // robot MQTT standard string generator
#include "WSS-SOCKETS.CPP" // robot security client based wss socket
#include "WSS-SENSOR.CPP" // robot sensor initialize & sensor publish
#include "WSS-PARSER.CPP" // JSON message parser
#include "WSS-TIMER.CPP" // main thread mqtt timer
#include "WSS-UDP.CPP" // robot udp receiver

#include "WSS-WSLOOP.CPP" // robot websocket main loop

void setup() {

  Serial.begin(115200);
  Serial.setTimeout(5);
  SERIAL_DEBUG("\n");
  WiFi.mode(WIFI_OFF);// ?
  //delay(2500);
  //delay(500);
  eepromInit();
#ifdef DEBUG_NODE_MCU
  eepromConfig.robotId = 1;
#endif

  if (eepromConfig.robotId > 0 && eepromConfig.robotId <= ROBOT_ID_MAX) {
    assignedClassroomCode = String(eepromConfig.classroom);
    assignedRobotId = eepromConfig.robotId;
  }
  gpioInit();

  sensorConfigure();

  if (!configRobotWireDebug) {
    servoControl(SERVO_RESET_ANGLE, SERVO_DELAY, true);
  }

#ifdef DEBUG_BLE_ON
  configRobotBleMode = initializeBle();
  if (configRobotBleMode && (!configRobotWireDebug)) {
    colorHeadlights[0].r = 10;
    colorHeadlights[0].g = 120;
    colorHeadlights[0].b = 120;
    colorHeadlights[1].r = 10;
    colorHeadlights[1].g = 120;
    colorHeadlights[1].b = 120;
    while (!rgbColorSendI2C());
    colorHeadlightsBrightness = 50;
    colorHeadlightsOnTime = 4;
    colorHeadlightsOffTime = 8;
    while (!rgbBlinkSendI2C());
    return;
  }
#endif
  wifiInit();
  if (configRobotWireDebug) {
    initUpdateServer();

    colorHeadlights[0].r = 10;
    colorHeadlights[0].g = 10;
    colorHeadlights[0].b = 120;
    colorHeadlights[1].r = 10;
    colorHeadlights[1].g = 10;
    colorHeadlights[1].b = 120;
    while (!rgbColorSendI2C());
    colorHeadlightsBrightness = 50;
    colorHeadlightsOnTime = 2;
    colorHeadlightsOffTime = 2;
    while (!rgbBlinkSendI2C());

    return;
  }
  if (wifiSsidScanned.startsWith(wifiSsidTestKeyworld)) {

    colorHeadlightsBrightness = 100;
    colorHeadlightsOnTime = 4;
    colorHeadlightsOffTime = 1;
    while (!rgbBlinkSendI2C());
    
    colorHeadlights[0].r = 255;
    colorHeadlights[0].g = 0;
    colorHeadlights[0].b = 0;
    colorHeadlights[1].r = 255;
    colorHeadlights[1].g = 0;
    colorHeadlights[1].b = 0;
    while (!rgbColorSendI2C());

    buzzerBeep(BUZZER_PIN, 500, 500, 20, 2);


    colorHeadlights[0].r = 0;
    colorHeadlights[0].g = 255;
    colorHeadlights[0].b = 0;
    colorHeadlights[1].r = 0;
    colorHeadlights[1].g = 255;
    colorHeadlights[1].b = 0;
    while (!rgbColorSendI2C());

    buzzerBeep(BUZZER_PIN, 500, 1000, 20, 2);

    colorHeadlights[0].r = 0;
    colorHeadlights[0].g = 0;
    colorHeadlights[0].b = 255;
    colorHeadlights[1].r = 0;
    colorHeadlights[1].g = 0;
    colorHeadlights[1].b = 255;
    while (!rgbColorSendI2C());
    buzzerBeep(BUZZER_PIN, 500, 1500, 20, 2);
    colorHeadlights[0].r = 255;
    colorHeadlights[0].g = 255;
    colorHeadlights[0].b = 255;
    colorHeadlights[1].r = 255;
    colorHeadlights[1].g = 255;
    colorHeadlights[1].b = 255;
    while (!rgbColorSendI2C());
    delay(200);

    uint8_t commandMotorLength = 2;
    int motorParameters[] = {836, 1092};
    int motorSteps[] = {61, 61};
    for (uint8_t i = 0; i < commandMotorLength; i++) {

      int commandMotorParameters = motorParameters[i];
      stepperMotorBuffer.steps[i] = motorSteps[i];
      stepperMotorBuffer.speedLeft[i] = (commandMotorParameters & 0xF0) / 16;
      stepperMotorBuffer.speedRight[i] = commandMotorParameters & 0x0F;
      switch (commandMotorParameters / 256) {
        case 1:
          stepperMotorBuffer.directionLeft[i] = 0;
          stepperMotorBuffer.directionRight[i] = 0;
          break;
        case 2:
          stepperMotorBuffer.directionLeft[i] = 3;
          stepperMotorBuffer.directionRight[i] = 3;
          break;
        case 3:
          stepperMotorBuffer.directionLeft[i] = 0;
          stepperMotorBuffer.directionRight[i] = 3;
          break;
        case 4:
          stepperMotorBuffer.directionLeft[i] = 3;
          stepperMotorBuffer.directionRight[i] = 0;
          break;
        default:
          stepperMotorBuffer.steps[i] = 0;
      }

      //stepperMotorBuffer.directionLeft[i] = 3;
      //stepperMotorBuffer.directionRight[i] = 0;
    }
    while (!stepperMotorSendI2C());

    delay(1000);
    servoControl(SERVO_MIN_ANGLE, SERVO_DELAY, true);
    delay(500);
    servoControl(SERVO_MAX_ANGLE, SERVO_DELAY, true);
    delay(500);
    servoControl(SERVO_MIN_ANGLE, SERVO_DELAY, true);
    delay(500);
    servoControl(SERVO_MAX_ANGLE, SERVO_DELAY, true);
    delay(10000);
    ESP.reset();
  }

  udpServer.begin(UDP_PORT);
  //bool testResult = wifiHttpTest();
  //while(!wsConnectTest("216.58.199.67", 80, 5000)); //1~20
  bool apiReuestResult = apiRequest(API_REQUEST_URL, API_REQUEST_PORT, 5000); // save time to EEPROM
  if (apiReuestResult) {
    wsStatus = WS_NOT_START;
    //buzzerBeep(BUZZER_PIN, 200, 1000, 10, 1);
    colorHeadlights[0].r = 20;
    colorHeadlights[0].g = 120;
    colorHeadlights[0].b = 120;
    while (!rgbColorSendI2C());
    //colorHeadlightsBrightness = 10;
    //colorHeadlightsOnTime = 16;
    //colorHeadlightsOffTime = 16;
    //while (!rgbBlinkSendI2C());
  }
  else {
    colorHeadlights[0].r = 120;
    colorHeadlights[0].g = 10;
    colorHeadlights[0].b = 10;
    while (!rgbColorSendI2C());
    buzzerBeep(BUZZER_PIN, 400, 1200, 20, 1);
    buzzerBeep(BUZZER_PIN, 400, 1200, 20, 1);

  }

  wssConnect();

}
//uint8_t color1 = 0;
//long lastTime = 0;
void loop() {
  long timeNowLoop = millis();
  batteryReadLoop(timeNowLoop);



  if (configRobotBleMode) {
#ifdef DEBUG_BLE_ON
    serialLoop();
#endif
  }
  else {

    if (configRobotWireDebug) {
      httpServer.handleClient();
    }
    else {

      rtttlLoop();
      wssMainLoop(timeNowLoop);
    }
  }

  realTimeProcessLoop();

  /*
    if (nowTime - lastTime >= 200) {

    colorHeadlights[0].r = color1;
    colorHeadlights[1].b = color1;
    while (!rgbColorSendI2C());
    if (msg_count == 255)msg_count = 0;
    else msg_count++;
    if (msg_count != msg_count_feedback) {
      buzzerBeep(BUZZER_PIN, 200, 500, 5, 1);
    }
    if (color1 >= 240)color1 = 0;
    else color1 = color1 + 10;

    lastTime = nowTime;
    }
  */

}
void interruptExternal() {
  if (externalInterruptFlag == 2) {
    externalInterruptFlag = 3;
  }
  /*
    //not quite sure
    if (SENSOR_photo_counter_enabled) {
    if (millis() - SENSOR_photo_counter_last_time >= 10) {
      SENSOR_photo_counter++;
      SENSOR_photo_counter_last_time = millis();
    }
    }
  */
}
