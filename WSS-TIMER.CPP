
#define ROBOT_HEARTBEAT_INTERVAL 5000
static long timeLastRobotHeartBeat = 0;

void mqttSendRobotHeartBeat(long timeNowGet) {
  if (timeNowGet - timeLastRobotHeartBeat >= ROBOT_HEARTBEAT_INTERVAL) {
    //while (!batteryReadSendI2C());
    //resetChargingESP();

    if (assignedRobotId > 0) {
      wsSendMqttPublish((String("to/classroom/") + assignedClassroomCode + String("/robot_status")).c_str(), (String("{\"id\":") + String(assignedRobotId) + String(",\"status\":") + String(1) + String(",\"battery\":") + String(batteryVoltageValue * 4) + String("}")).c_str());
    }
    else if (udpClassroomCodeSavedCount > 0 && assignedRobotId == 0) {
      //wsSendMqttPublish((String("to/classroom/") + udpClassroomCodeBuffer[udpClassroomCodeBufferIndex] + String("/register")).c_str(), (String("{\"mac_broadcast\":\"") + WiFi.macAddress() + String("\",\"ip\":\"") + WiFi.localIP().toString() + String("\"}")).c_str());
      wsSendMqttPublish((String("to/classroom/") + udpClassroomCodeBuffer[udpClassroomCodeBufferIndex] + String("/register")).c_str(), (String("{\"mac_broadcast\":\"") + WiFi.macAddress() + String("\",\"ip\":\"") + WiFi.localIP().toString() + String("\"}")).c_str());
      udpClassroomCodeBufferIndex++;
      if (udpClassroomCodeBufferIndex >= udpClassroomCodeSavedCount) {
        udpClassroomCodeBufferIndex = 0;
      }

    }
    timeLastRobotHeartBeat = timeNowGet;
  }
}

#define ROBOT_BATTERY_READ_INTERVAL 5000
static long timeLastBatteryRead = 0;

void batteryReadLoop(long timeNowGet) {
  if (timeNowGet - timeLastBatteryRead >= ROBOT_BATTERY_READ_INTERVAL) {
    while (!batteryReadSendI2C());
    resetChargingESP();

    timeLastBatteryRead = timeNowGet;
  }
}

static long timeLastSensorPublished = 0;
void mqttPublishSensorData(long timeNowGet) {
  if (assignedRobotId > 0) {
    if (timeNowGet - timeLastSensorPublished >= sensorPublishInterval) {
      String a = sensorPayloadGenerate();
      if (a.length() > 0) {
        wsSendMqttPublish((String("to/classroom/") + assignedClassroomCode + String("/sensors_update")).c_str(), a.c_str());
      }
      timeLastSensorPublished = timeNowGet;
    }
  }
}
static long timeLastSensorRefreshed = 0;
static long timeLastI2CRefreshed = 0;
static bool lastI2CRefreshedSwapFlag = false;

void refreshSensor(long timeNowGet) {
  if (assignedRobotId > 0) {
    if (timeNowGet - timeLastSensorRefreshed >= sensorRefreshInterval) {
      if (eepromConfig.sensors[0] == SENSOR_UNIQUE_ID_LED) {
        if (sensorRefreshFlag == 3) {
          pinMode(DIGITAL_PIN, OUTPUT);
          analogWriteRange(100);
          analogWriteFreq(2000);
          analogWrite(DIGITAL_PIN, sensorLEDDuty);
        }
        else if (sensorRefreshFlag == 1) {
          pinMode(DIGITAL_PIN, OUTPUT);
          analogWriteRange(100);
          analogWriteFreq(2000);
          analogWrite(DIGITAL_PIN, sensorLEDDuty);
          sensorRefreshFlag = 2;
        }
        else if (sensorRefreshFlag == 2) {
          pinMode(DIGITAL_PIN, OUTPUT);
          analogWrite(DIGITAL_PIN, 0);
          sensorRefreshFlag = 1;
        }
      }

      if (eepromConfig.sensors[0] == SENSOR_UNIQUE_ID_RGB_LED) {
        if (sensorRefreshFlag == 3) {
          ledPixelsRefresh(true);
        }

        else if (sensorRefreshFlag == 1) {
          if (ledPixelsTransformIndex < 3) {
            ledPixelsMatrixMultiply(ledPixelsTransformMatrix[ledPixelsTransformIndex]);
          }

          ledPixelsRefresh(true);
          sensorRefreshFlag = 2;
        }

        else if (sensorRefreshFlag == 2) {
          if (ledPixelsTransformIndex < 3) {
            ledPixelsMatrixMultiply(ledPixelsTransformMatrix[ledPixelsTransformIndex]);
            ledPixelsRefresh(true);
          }
          else if (ledPixelsTransformIndex == 3) {
            ledPixelsRefresh(false);
          }

          sensorRefreshFlag = 1;
        }
      }

      timeLastSensorRefreshed = timeNowGet;

    }
    if (timeNowGet - timeLastI2CRefreshed >= sensorI2CInterval) {

      if (eepromConfig.sensors[1] == SENSOR_UNIQUE_ID_4_DIGI_DISPLAY) {
        bool showColonFlag = false;
        if (digiDisplayColon) {
          showColonFlag = digiDisplayColon;
          digiDisplayColon = lastI2CRefreshedSwapFlag;
        }

        if (digiDisplayCountdownFlag && lastI2CRefreshedSwapFlag) {
          if (digiDisplayCountdownSec == 0 && digiDisplayCountdownMin > 0) {

            digiDisplayBuffer[0] = '0' + digiDisplayCountdownMin / 10;
            digiDisplayBuffer[1] = '0' + (digiDisplayCountdownMin % 10);
            digiDisplayBuffer[2] = '0' + digiDisplayCountdownSec / 10;
            digiDisplayBuffer[3] = '0' + (digiDisplayCountdownSec % 10);

            digiDisplayCountdownMin--;
            digiDisplayCountdownSec = 59;
          }
          else if (digiDisplayCountdownSec > 0) {
            digiDisplayBuffer[0] = '0' + digiDisplayCountdownMin / 10;
            digiDisplayBuffer[1] = '0' + (digiDisplayCountdownMin % 10);
            digiDisplayBuffer[2] = '0' + digiDisplayCountdownSec / 10;
            digiDisplayBuffer[3] = '0' + (digiDisplayCountdownSec % 10);

            digiDisplayCountdownSec--;
          }
          else if (digiDisplayCountdownSec == 0 && digiDisplayCountdownMin == 0) {

            digiDisplayBuffer[0] = '0';
            digiDisplayBuffer[1] = '0';
            digiDisplayBuffer[2] = '0';
            digiDisplayBuffer[3] = '0';
            wsSendMqttPublish((String("to/classroom/") + assignedClassroomCode + String("/interrupt")).c_str(), String("{\"R\":" + String(assignedRobotId) + ", \"I\": {\"S" + String(eepromConfig.sensors[1]) + "\": " + String(1) + "}}").c_str());
            digiDisplayCountdownFlag = false;
          }

        }

        digiDisplayRefresh();
        digiDisplayColon = showColonFlag;
      }

      else if (eepromConfig.sensors[1] == SENSOR_UNIQUE_ID_MATRIX_DISPLAY) {
        if (matrixDisplayProgress <= 100) {
          matrixDisplayDrawProgress(matrixDisplayProgress);
        }
        else if (matrixDisplaySrollText.length() > 0) {
          if (matrixDisplaySrollText.length() == 1) {
            matrixDisplayStringScroll(7, matrixDisplaySrollText);
          }
          else {
            matrixDisplayStringScroll(matrixDisplaySrollOffset, matrixDisplaySrollText);
            matrixDisplaySrollOffset--;
            if (matrixDisplaySrollOffset == 0) {
              matrixDisplaySrollOffset = matrixDisplayStartBit + matrixDisplaySrollText.length() * 6;
            }
          }
        }
        else {
          matrixDisplayDrawPixels();
        }
      }
      else if (eepromConfig.sensors[1] == SENSOR_UNIQUE_ID_GESTURE) {
        if (i2cAddressNotValid(I2C_ADDRESS_GESTURE)) {
          i2CInitializeFlag = false;
          //break;
        }
        else {
          if (!i2CInitializeFlag) {
            gestureInit();
            i2CInitializeFlag = true;
          }
          uint8_t gestureValue = waitGestures();
          if (gestureValue > 0) {
            wsSendMqttPublish((String("to/classroom/") + assignedClassroomCode + String("/interrupt")).c_str(), String("{\"R\":" + String(assignedRobotId) + ",\"I\":{\"S63\":" + String(gestureValue) + "}}").c_str());
          }
          /*
          
          if (gestureDataIndex < 2) {
            uint8_t error = 0;//paj7620ReadReg(0x43, 1, &gestureData[gestureDataIndex]);
            if (!error)
            {
              gestureDataIndex++;
            }
          }
          
          
          if (gestureDataIndex == 1) {
            uint8_t gentureResult = 0;
            switch (gestureData[0])
            {
              case GES_RIGHT_FLAG:
                {
                  gestureDataIndex++;
                  break;
                }
              case GES_LEFT_FLAG:
                {
                  gestureDataIndex++;
                  break;
                }
              case GES_UP_FLAG:
                {
                  gestureDataIndex++;
                  break;
                }
              case GES_DOWN_FLAG:
                {
                  gestureDataIndex++;
                  break;
                }
              case GES_FORWARD_FLAG:
                {
                  gentureResult = SENSOR_GESTURE_FORWARD;
                  break;
                }
              case GES_BACKWARD_FLAG:
                {
                  gentureResult = SENSOR_GESTURE_BACKWARD;
                  break;
                }
              case GES_CLOCKWISE_FLAG:
                {
                  gentureResult = SENSOR_GESTURE_CLOCKWISE;
                  break;
                }
              case GES_COUNT_CLOCKWISE_FLAG:
                {
                  gentureResult = SENSOR_GESTURE_ANTICLOCKWISE;
                  break;
                }
              default:
                {
                  gestureDataIndex = 0;
                }
            }
            if (gentureResult > 0) {
              gestureDataIndex = 0;
              //wsSendMqttPublish((String("to/classroom/") + assignedClassroomCode + String("/interrupt")).c_str(), String("{\"R\":" + String(assignedRobotId) + ",\"I\":{\"S63\":" + String(gentureResult) + "}}").c_str());
            }
          }

          else if (gestureDataIndex == 2) {

            uint8_t gentureResult = 0;
            switch (gestureData[0])
            {
              case GES_RIGHT_FLAG:
                {
                  if (gestureData[1] == GES_FORWARD_FLAG)
                  {
                    gentureResult = SENSOR_GESTURE_FORWARD;

                  }
                  else if (gestureData[1] == GES_BACKWARD_FLAG)
                  {
                    gentureResult = SENSOR_GESTURE_BACKWARD;
                  }
                  else
                  {
                    gentureResult = SENSOR_GESTURE_RIGHT;
                  }
                  break;
                }
              case GES_LEFT_FLAG:
                {
                  if (gestureData[1] == GES_FORWARD_FLAG)
                  {
                    gentureResult = SENSOR_GESTURE_FORWARD;

                  }
                  else if (gestureData[1] == GES_BACKWARD_FLAG)
                  {
                    gentureResult = SENSOR_GESTURE_BACKWARD;
                  }
                  else
                  {
                    gentureResult = GES_LEFT_FLAG;
                  }
                  break;
                }
              case GES_UP_FLAG:
                {
                  if (gestureData[1] == GES_FORWARD_FLAG)
                  {
                    gentureResult = SENSOR_GESTURE_FORWARD;

                  }
                  else if (gestureData[1] == GES_BACKWARD_FLAG)
                  {
                    gentureResult = SENSOR_GESTURE_BACKWARD;
                  }
                  else
                  {
                    gentureResult = GES_UP_FLAG;
                  }
                  break;
                }
              case GES_DOWN_FLAG:
                {
                  if (gestureData[1] == GES_FORWARD_FLAG)
                  {
                    gentureResult = SENSOR_GESTURE_FORWARD;

                  }
                  else if (gestureData[1] == GES_BACKWARD_FLAG)
                  {
                    gentureResult = SENSOR_GESTURE_BACKWARD;
                  }
                  else
                  {
                    gentureResult = GES_DOWN_FLAG;
                  }
                  break;
                }
              default:
                {
                  gestureDataIndex = 0;
                }
            }
            if (gentureResult > 0) {
              //wsSendMqttPublish((String("to/classroom/") + assignedClassroomCode + String("/interrupt")).c_str(), String("{\"R\":" + String(assignedRobotId) + ",\"I\":{\"S63\":" + String(gentureResult) + "}}").c_str());
              gestureDataIndex = 0;
            }
            //wsSendMqttPublish((String("to/classroom/") + assignedClassroomCode + String("/interrupt")).c_str(), String("{\"R\":" + String(assignedRobotId) + ",\"I\":{\"S63\":" + String(gestureData[gestureDataIndex]) + "}}").c_str());
          }
          */
        }
      }
      lastI2CRefreshedSwapFlag = !lastI2CRefreshedSwapFlag;
      timeLastI2CRefreshed = timeNowGet;
    }
  }
}
/*
  #define ROBOT_BATTERY_DETECT_INTERVAL 5000
  static long timeLastReadBattery = 0;
  void readBattery(long interval, long timeNowGet) {
  if (timeNowGet - timeLastReadBattery >= interval && wsStatus >= WS_MQTT_CONNECTED) {
    while(!batteryReadSendI2C);
    timeLastReadBattery = timeNowGet;
  }
  }
*/
void realTimeProcessLoop() {
  if (commandStatusAsync == 2) {
    commandStatusAsync = 0;
    if (wsStatus >= WS_MQTT_CONNECTED) {
      if (commandIdSavedInterrupt.length() > 0) {
        wsSendMqttPublish((String("to/classroom/") + assignedClassroomCode + String("/command_status")).c_str(), (String("{\"id\":\"") + commandIdSavedInterrupt + String("\",\"index\":") + String(commandIndexSavedInterrupt) + String(",\"finished\":0,\"p\":2}")).c_str());
        commandIdSavedInterrupt = "";
      }
      if (commandIdSavedNormal.length() > 0) {
        wsSendMqttPublish((String("to/classroom/") + assignedClassroomCode + String("/command_status")).c_str(), (String("{\"id\":\"") + commandIdSavedNormal + String("\",\"index\":") + String(commandIndexSavedNormal) + String(",\"finished\":0,\"p\":1}")).c_str());
        commandIdSavedNormal = "";
      }
    }
    else if (configRobotBleMode) {
      if (commandIdSavedInterrupt.length() > 0) {
        SERIAL_BLE_SEND("F=%s\n", commandIdSavedInterrupt.c_str());
        commandIdSavedInterrupt = "";
      }
      if (commandIdSavedNormal.length() > 0) {
        SERIAL_BLE_SEND("F=%s\n", commandIdSavedNormal.c_str());
        commandIdSavedNormal = "";
      }
    }
  }
  else if (commandStatusAsync == 4) {
    if (digitalRead(MINI_EVENT_PIN) == 1) {
      commandStatusAsync = 5;
      timeLastMotorFinished = millis();
      //buzzerBeep(BUZZER_PIN, 500, 2500, 10, 1);
    }
  }
  else if (commandStatusAsync == 5) {
    if (millis() - timeLastMotorFinished >= MOTOR_COMMAND_DELAY) {
      commandStatusAsync = 2;
    }
  }


  if (externalInterruptFlag == 4) {
    if (millis() - timeLastInterruptSent >= 500) {
      externalInterruptFlag = 2;
    }
  }
  if (externalInterruptFlag == 3) {
    if (wsStatus >= WS_MQTT_CONNECTED) {
      wsSendMqttPublish((String("to/classroom/") + assignedClassroomCode + String("/interrupt")).c_str(), String("{\"R\":" + String(assignedRobotId) + ", \"I\": {\"S" + String(eepromConfig.sensors[0]) + "\": " + String(1) + "}}").c_str());
    }
    timeLastInterruptSent = millis();
    externalInterruptFlag = 4;
  }
  if (externalInterruptFlag == 5) {
    unsigned long irCode = irDecode();
    if (irCode > 0 && irCode < 0xFFFFFFFF) {
      if (wsStatus >= WS_MQTT_CONNECTED) {
        wsSendMqttPublish((String("to/classroom/") + assignedClassroomCode + String("/interrupt")).c_str(), String("{\"R\":" + String(assignedRobotId) + ", \"I\": {\"S" + String(eepromConfig.sensors[0]) + "\": " + String(irCode) + "}}").c_str());
        wsSendMqttPublish((String("to/classroom/") + assignedClassroomCode + String("/sensors_update")).c_str(), String("{\"R\":" + String(assignedRobotId) + ", \"S\": {\"S" + String(eepromConfig.sensors[0]) + "\": " + String(irCode) + "}, \"STATUS\": {\"S" + String(eepromConfig.sensors[0]) + "\": 1}}").c_str());
      }
    }
  }

  if (analogArrayIndex == (ANALOG_SAMPLE_HALF * 2)) {

    blinker.detach();
    analogArrayIndex = 0;
    if (wsStatus >= WS_MQTT_CONNECTED) {
      for (uint8_t i = 0; i < 2; i++) {
        String a = uint16ArrayToString(analogArray[i], ANALOG_SAMPLE_HALF);
        wsSendMqttPublish((String("to/classroom/") + assignedClassroomCode + String("/sensors_update")).c_str(), String("{\"R\":" + String(assignedRobotId) + ",\"S\":{\"S47\":\"" + a + "\"},\"STATUS\":{\"S47\":" + String(i + 1) + "}}").c_str());
      }
    }
    commandStatusAsync = 2;
  }
}



